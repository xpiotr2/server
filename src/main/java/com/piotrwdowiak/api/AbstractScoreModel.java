/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piotrwdowiak.api;

import java.sql.SQLException;
import java.util.List;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author piotr
 */
public interface AbstractScoreModel {
    
    List <Score> getScores() throws SQLException, ClassNotFoundException;
    List <Score> getSortedBestScores(int rowNumber) throws SQLException, ClassNotFoundException;
    boolean addScore(Score score) throws SQLException, ClassNotFoundException;
    
}
