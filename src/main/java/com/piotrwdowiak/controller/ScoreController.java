/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piotrwdowiak.controller;

import com.piotrwdowiak.api.Score;
import com.piotrwdowiak.persistance.ScoreModel;
import com.piotrwdowiak.api.AbstractScoreModel;
import java.sql.SQLException;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author piotr
 */
@Controller
public class ScoreController {

    @RequestMapping(value = "/bestScores", method = RequestMethod.GET)
    public @ResponseBody
    List<Score> getBestScores(@RequestParam(value = "number", defaultValue = "10") int number) throws SQLException, ClassNotFoundException {
        AbstractScoreModel scoreModel = new ScoreModel();
        return scoreModel.getSortedBestScores(number);
    }

    @RequestMapping(value = "/scores", method = RequestMethod.GET)
    public @ResponseBody
    List<Score> getAllScores() throws SQLException, ClassNotFoundException {
        AbstractScoreModel scoreModel = new ScoreModel();
        return scoreModel.getScores();
    }

    @RequestMapping(value = "/addScoreASD", method = RequestMethod.PUT)
    public ResponseEntity<Score> update(
            @RequestParam("name") String name,
            @RequestParam("points") int points
    ) throws SQLException, ClassNotFoundException {

        AbstractScoreModel scoreModel = new ScoreModel();
        boolean operation = scoreModel.addScore(new Score(0, name, points));

        if (operation) {
            return new ResponseEntity<Score>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Score>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
