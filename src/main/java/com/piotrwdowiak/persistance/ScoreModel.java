/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piotrwdowiak.persistance;

import com.piotrwdowiak.api.Score;
import com.piotrwdowiak.api.AbstractScoreModel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author piotr
 */
public class ScoreModel implements AbstractScoreModel {

    @Override
    public List<Score> getScores() throws SQLException, ClassNotFoundException {
        return selectFromScores("SELECT * FROM score_list");
    }
    
    @Override
    public List<Score> getSortedBestScores(int rowNumber) throws SQLException, ClassNotFoundException {
        return selectFromScores("SELECT * FROM score_list order by points desc limit " + rowNumber + ";");
    }

    @Override
    public boolean addScore(Score score) throws SQLException, ClassNotFoundException {
        Connection c = DBConnection.getConnection();

        String sql = "INSERT INTO score_list (`name`, `points`) VALUES ('" + score.getName() + "', '" + score.getPoints() + "');";
        Statement stmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        int result = stmt.executeUpdate(sql);

        if (result == 0) {
            return false;
        } else {
            return true;
        }

    }

    private List<Score> selectFromScores(String queryString) throws SQLException, ClassNotFoundException {
        Connection c = DBConnection.getConnection();

        Statement stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery(queryString);

        List<Score> result = new ArrayList<Score>();

        while (rs.next()) {
            Score score = new Score(rs.getInt("id"), rs.getString("name"), rs.getInt("points"));
            result.add(score);
        }

        c.close();
        return result;
    }
}
