/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piotrwdowiak.persistance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    

    public static String MYSQL_USERNAME = System.getenv("OPENSHIFT_MYSQL_DB_USERNAME");
    public static String MYSQL_PASSWORD = System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
    public static String MYSQL_DATABASE_HOST = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
    public static String MYSQL_DATABASE_PORT = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
    public static String MYSQL_DATABASE_NAME = "memoryServer";

    public DBConnection() {

    }

    //public static final String MYSQL_DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static Connection con;

    /**
     * returns connection to DB
     *
     * @param con Connection
     */
    public static Connection getConnection() throws ClassNotFoundException {

        Class.forName("com.mysql.jdbc.Driver");

        boolean isTestedLocally = false;
        
        if (isTestedLocally) {
            MYSQL_USERNAME = "root";
            MYSQL_PASSWORD = "root";
            MYSQL_DATABASE_HOST = "127.0.0.1";
            MYSQL_DATABASE_PORT = "3306";
            MYSQL_DATABASE_NAME = "memoryServer";
        }

        String url = "";
        //  if(con==null){
        try {
            url = "jdbc:mysql://" + MYSQL_DATABASE_HOST + ":" + MYSQL_DATABASE_PORT + "/" + MYSQL_DATABASE_NAME;
            con = DriverManager.getConnection(url, MYSQL_USERNAME, MYSQL_PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        //    }
        return con;
    }
}
